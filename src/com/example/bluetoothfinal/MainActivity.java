package com.example.bluetoothfinal;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Set;
import java.util.UUID;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends Activity {
	private String mac;
	private BluetoothAdapter mBluetoothAdapter;
	private BluetoothDevice device;
	private BluetoothSocket socket;
	private Button on;
	private Button off;
	private Button scan;
	private Button send;
	private Button list;
	private Button clear;
	private ButtonClicked click;
	private TextView status;
	private ItemClickDetected itemClickDetected;
	private ItemClickBound itemClickBound;
	private ListView listDeviceDectected;
	private ListView listDeviceBound;
	private ArrayAdapter<String> BTDeviceShow;
	private Set<BluetoothDevice> BTAdapterBound;
	private UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
	public OutputStream out;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		status = (TextView) findViewById(R.id.textView1);
		mBluetoothAdapter= BluetoothAdapter.getDefaultAdapter();
		if(mBluetoothAdapter == null){
			on.setEnabled(false);
			off.setEnabled(false);
			scan.setEnabled(false);
			send.setEnabled(false);
			list.setEnabled(false);
			clear.setEnabled(false);
			status.setText("Bluetooth no detectado");
		}else{
			click=new ButtonClicked();
			itemClickDetected = new ItemClickDetected();
			itemClickBound = new ItemClickBound();
			on=(Button) findViewById(R.id.on);
			off=(Button) findViewById(R.id.off);
			scan=(Button) findViewById(R.id.scan);
			send=(Button) findViewById(R.id.send);
			list=(Button) findViewById(R.id.list);
			clear=(Button) findViewById(R.id.clear);
			on.setOnClickListener(click);
			off.setOnClickListener(click);
			scan.setOnClickListener(click);
			send.setOnClickListener(click);
			list.setOnClickListener(click);
			clear.setOnClickListener(click);
			
			
			listDeviceDectected = (ListView)findViewById(R.id.detected);
			listDeviceBound = (ListView) findViewById(R.id.bound);
			listDeviceBound.setOnItemClickListener(itemClickBound);
			listDeviceDectected.setOnItemClickListener(itemClickDetected);;
		    BTDeviceShow= new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
		    listDeviceDectected.setAdapter(BTDeviceShow);
		    listDeviceBound.setAdapter(BTDeviceShow);
		}
		
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	public void onBluetooth(){
		if (mBluetoothAdapter.isEnabled()){
			status.setText("El bluetooth ya esta activado");
		}else{
			status.setText("Preparando activaci�n");
			Intent enable = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			 startActivityForResult(enable, 1);
			 status.setText("Bluetooth activado");
		}
	}
	public void offBluetooth(){
		if (!mBluetoothAdapter.isEnabled()){
			status.setText("El bluetooth ya esta desactivado");
		}else{
			status.setText("Preparando desactivaci�n");
			mBluetoothAdapter.disable();
			 status.setText("Bluetooth desactivado");
		}
	}
	final BroadcastReceiver bReceiver = new BroadcastReceiver() {
	    public void onReceive(Context context, Intent intent) {
	    	
	        String action = intent.getAction();
	        if (BluetoothDevice.ACTION_FOUND.equals(action)) {
	        	 BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
	             BTDeviceShow.add(device.getName() + "\n" + device.getAddress());
	             BTDeviceShow.notifyDataSetChanged();
	        }
	        if (BTDeviceShow.isEmpty()){
				status.setText("Dispositivos Bluetooth no entrados");
			}else{
				status.setText("Dispositivos Bluetooth entrados");
			}
	        listDeviceDectected.setVisibility(ListView.VISIBLE);
	    }
	};
	public void scanBluetooth(){
		listDeviceBound.setVisibility(ListView.GONE);
		status.setText("Buscado dispositivos Bluetooth");
		BTDeviceShow.clear();
		if (mBluetoothAdapter.isDiscovering()) {
			mBluetoothAdapter.cancelDiscovery();
		}
		else {
			mBluetoothAdapter.startDiscovery();
			registerReceiver(bReceiver, new IntentFilter(BluetoothDevice.ACTION_FOUND));
			
		}
		
	}
	public void listBound(){
		listDeviceDectected.setVisibility(ListView.GONE);
		BTAdapterBound = mBluetoothAdapter.getBondedDevices();
		BTDeviceShow.clear();
	    for(BluetoothDevice device : BTAdapterBound)
	    	  BTDeviceShow.add(device.getName()+ "\n" + device.getAddress());
	    
	    if (BTDeviceShow.isEmpty()){
			status.setText("Dispositivos Bluetooth no entrados");
		}else{
			status.setText("Dispositivos Bluetooth entrados");
		}
		listDeviceBound.setVisibility(ListView.VISIBLE);
	}
	public void clear(){
		if (BTDeviceShow !=null && !BTDeviceShow.isEmpty()){
			BTDeviceShow.clear();
		}
		if (BTAdapterBound !=null && !BTAdapterBound.isEmpty()){
			BTAdapterBound.clear();
		}
		status.setText("lista limpia");
	}
	public void connect(){
		//device = mBluetoothAdapter.getRemoteDevice("E8:CD:2D:13:2D:EB");//ivan
		//device = mBluetoothAdapter.getRemoteDevice("DC:A9:71:70:13:A3");//pc
		//device = mBluetoothAdapter.getRemoteDevice("44:D4:E0:B2:A5:45");//yo
		//device = mBluetoothAdapter.getRemoteDevice("E0:B9:A5:6C:CB:94");//tblet
		//device = mBluetoothAdapter.getRemoteDevice("B4:52:7E:C0:12:C5");//deborah
		device= mBluetoothAdapter.getRemoteDevice(mac);
		
		try {
			socket = device.createInsecureRfcommSocketToServiceRecord(uuid);
			mBluetoothAdapter.cancelDiscovery();
			socket.connect();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void send(){
		connect();
		if (socket.isConnected()){
			status.setText("conectado");
			//as� se manda salio el cuadro para pulsar
			/*Intent intent = new Intent();
			intent.setAction(Intent.ACTION_SEND);
			intent.setType("text/plain");
			//esto es la principal
			intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(Environment.getExternalStorageDirectory().getPath()+"/white.txt")) );
			startActivity(intent);*/
			try {
				out = socket.getOutputStream();
				/*Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.white);
	    	    ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    	    bm.compress(Bitmap.CompressFormat.PNG, 100,baos); //bm is the bitmap object
	    	    byte[] b = baos.toByteArray();*/
	    	    out.write("hola mund0".getBytes());
	    	    
	    	    socket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			
			
		}else{
			status.setText("No conectado");
		}
	}
	class ItemClickBound implements OnItemClickListener{

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int post, long id){
			// TODO Auto-generated method stub
			String mac = BTDeviceShow.getItem(post).split("\n")[1];
			device = mBluetoothAdapter.getRemoteDevice(mac);
			MainActivity.this.mac=mac;
			/*if (device.getBondState()==BluetoothDevice.BOND_BONDED){
			
				try {
					Class<?> btClass = Class.forName("android.bluetooth.BluetoothDevice");
					Method removeBondMethod = btClass.getMethod("removeBond");  
					removeBondMethod.invoke(device);
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NoSuchMethodException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}*/
		}
		
	}
	class ItemClickDetected implements OnItemClickListener{
		public void onItemClick(AdapterView<?> parent, View view, int post, long id) {
			String mac = BTDeviceShow.getItem(post).split("\n")[1];
			device = mBluetoothAdapter.getRemoteDevice(mac);
			if (device.getBondState()==BluetoothDevice.BOND_NONE){
				try {
					//device.createBond(); requiere api 19 minimo
					Class<?> class1 = Class.forName("android.bluetooth.BluetoothDevice");
					Method createBondMethod = class1.getMethod("createBond");  
					createBondMethod.invoke(device);  
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NoSuchMethodException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	class ButtonClicked implements OnClickListener{

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.on:
				onBluetooth();
				break;
			case R.id.off:
				offBluetooth();
				break;
			case R.id.scan:
				scanBluetooth();
				break;
			case R.id.send:
				send();
				try {
					socket.close();
					status.setText("Finalizado");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			case R.id.list:
				listBound();
				break;
			case R.id.clear:
				clear();
				break;
			default:
				break;
			}
		}
	}
	
}
